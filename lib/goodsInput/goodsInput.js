/**
 * Created by mooshroom on 2015/12/12.
 */
define([
    "avalon",
    "text!.. /../lib/goodsInput/goodsInput.html",
    '../../lib/goodsInput/getInputPosition',
    "css!../../lib/goodsInput/goodsInput.css"
], function (avalon, html) {
    //声明新生成组件的ID
    avalon.component("tsy:goodsinput",{

        $init: function (vm,elem) {       //组件加载成功后自动执行
            //快捷键
            vm.$goodsHotKey={
                $opt:{
                    type:"keydown"
                },
                "up": function () {
                    if(vm.focusGoods>0){
                        vm.focusGoods--
                    }
                    else{
                        vm.focusGoods=vm.goods.length-1
                    }
                },
                "down":function(){
                    if(vm.focusGoods<vm.goods.length-1){
                        vm.focusGoods++
                    }
                    else{
                        vm.focusGoods=0
                    }
                },
                'left':function(){
                    if(vm.GP>1){
                        vm.pagerGoods(1)
                    }

                },
                "right": function () {
                    if(vm.GP<(Math.ceil(vm.GT/vm.GN)))
                        vm.pagerGoods(-1)
                },
                'enter':function(){
                    if(vm.focusGoods!=-1){
                        vm.jump2Goods(vm.focusGoods)
                    }
                }
            }
//选中商品
            vm.selectGoods=function (index) {
                vm.focusGoods = index
            };
            //执行回调
            vm.jump2Goods=function (index) {

                vm.callback(vm.goods[index])
                vm.goReset()
                vm.mustOut()

            };
            vm.mustOut= function () {
                vm.showing=vm.hovering=false
            }
            vm.goReset= function () {
                vm.goods = []
                vm.GP = 1
                vm.GT = 0
                vm.goodsKey = ""
                vm.goLastKey = ""
                vm.focusGoods = -1
                vm.onGoods = false
                vm.goodsWaiting=false
                vm.goodsDone=false
                vm.thatIsTM=''
            };
            vm.posePanel=function(ele){
                var p=cursor.getInputPositon(ele)
                vm.left= p.left
                vm.top= p.bottom
                vm.searchGoods(ele.value)
                vm.show(true)
            }

            vm.show= function (bool) {
                vm.showing=bool
                if(bool){
                    bindK(vm.$goodsHotKey)
                }else{
                    removeK(vm.$goodsHotKey)
                }
            }
            vm.goHover=function (i) {
                vm.hovering=i

            }

            //vm.goOut=function (i) {
            //    vm.onGoods = 0
            //}
            vm.searchGoods=function (key) {
                if(key==""){
                    //重置
                    vm.goReset()
                }else if(key!=vm.goLastKey){
                    vm.GP = 1
                    vm.GT = 0
                    /*
                     * 判断是否为条码枪输入的方法：
                     * 1. 快速输入，
                     * 2. 输入结果>=5&&<=30
                     * 3. 输入结果符合条码正则表达式
                     * 4. 返回结果有且只有一个
                     * */
                    vm.goodsKey=vm.goLastKey=key


                    //之前有延迟的
                    vm.inputTimes++ //用于连击判断的连击累计
                    console.log("*"+vm.inputTimes+"连击")
                    clearTimeout(vm.searchTimeOut)

                    //条码输入第一层判断
                    reg = new RegExp(/[A-Za-z\/0-9]{5,30}/g)//判断条码的表达式
                    if((vm.inputTimes>=5&&vm.inputTimes<=30)&&reg.test(key)){
                        //初步判断是条码输入
                        console.log("初步判断是条码输入:"+key)
                        vm.thatIsTM=key
                        vm.searchTimeOut=setTimeout(function(){

                            vm.callGoods()
                            vm.searchTimeOut=0
                        },80)//为反复测试之后，刚好在人肉输入的间隙，不会导致过量请求的值
                    }else{
                        //人肉输入
                        vm.searchTimeOut=setTimeout(function(){
                            vm.callGoods()
                            vm.searchTimeOut=0
                        },80)
                    }


                }
            };

            vm.pagerGoods= function (n) {
                var newGP = vm.GP + -n;
                if (newGP >= 1) {
                    vm.GP = newGP
                }
                else {
                    vm.GP = 1
                }
                clearTimeout(vm.searchTimeOut)
                vm.searchTimeOut=setTimeout(function(){
                    vm.callGoods()
                    vm.searchTimeOut=0
                },80)
            };
            //正是召唤商品列表
            vm.callGoods= function () {
                vm.inputTimes=0
                vm.goodsWaiting=true
                if (vm.GP < 1) {
                    vm.GP = 1
                }
                try{
                    vm.$customerID=bill.customer.CustomerID
                }catch(err){console.log(err.message)}

                //组装请求参数
                var data
                if(vm.$customerID!=""){
                    data={
                        keyword: vm.goodsKey,
                        P: vm.GP,
                        N: vm.GN,
                        CustomerID: vm.$customerID
                    }
                }else{
                    data={
                        keyword: vm.goodsKey,
                        P: vm.GP,
                        N: vm.GN,
                    }
                }

                //发起请求
                ws.call({
                    i: "Goods/search",
                    data: data,
                    success: function (res) {
                        vm.goodsDone=true
                        if(vm.GP == res.P){
                            if (res.L.length && vm.goodsKey != '') {
                                //vm.GP = res.P


                                var list = []
                                var resL = res.L
                                var len = resL.length

                                for (var i = 0; i < len; i++) {

                                    var All = 0;
                                    //当前库房获取逻辑,

                                    if (resL[i].Store) {
                                        for (var o = 0; o < resL[i].Store.length; o++) {
                                            if (resL[i].Store[o].StoreID == quickStart.nowStore.StoreID) {
                                                All = resL[i].Store[o].Amount
                                                break
                                            }
                                        }
                                    }


                                    var go = {
                                        GoodsID: resL[i].GoodsID,
                                        Name: resL[i].Name,
                                        ThisTotle: All,
                                        AllTotle: resL[i].StoreTotal,
                                        Price1: resL[i].Price1,
                                        Price0: resL[i].Price0,
                                        Standard: resL[i].Standard,
                                        Unit: resL[i].Unit,
                                        os: resL[i].Standard,
                                        ou: resL[i].Unit,
                                        LP: resL[i].LP,
                                        BarCode:resL[i].BarCode,
                                    }
                                    list.push(go)


                                }


                                vm.goods = list
                                /***判断是否为条形码***/
                                if(res.L.length == 1&&vm.thatIsTM!=""&&vm.thatIsTM==res.L[0].BarCode){

                                    vm.callbackTM(list[0])
                                    //vm.goodsKey = '';
                                    //vm.thatIsTM = "";       //是否为条形码
                                    vm.goReset()

                                    return;
                                }
                            } else {
                                vm.goods = []
                                //vm.GP--

                            }
                            vm.GT = res.T
                        }
                        vm.goodsWaiting=false


                    }
                })
            }

            window[this.id]=vm

        },
        $template:html,

        onInit: function () {

        },
        /*配置区域*/
        //选取之后的回调函数
        callback: function (goods) {
            console.log("尚未配置商品搜索组件：" +this.id+"的回调函数")
        },
        //条码枪返回之后的回掉函数
        callbackTM: function (goods) {
            console.log("尚未配置商品搜索组件：" +this.id+"的条码枪回调函数，将执行默认回调")
            this.callback(goods)
        },

        //暴露出来的名字
        id: "goodsInput",
        $customerID:"",
        /*内部区域*/

        //控制面板位置
        left:'',
        top:'',

        //控制面板的显示
        showing:false,

        hovering:false,
        goHover:function(){},
        selectGoods: function () {},





        // 商品搜索
        goodsWaiting:false,
        goodsDone:false,
        goods: [],
        goodsKey: "",
        goLastKey: "",
        focusGoods: -1,
        onGoods: false,
        pagerGoods:function(){},
//        lastInput:0,
        GP: 1,
        GT: 0,
        GN: 8,
        /*为判断条码创造的变量*/
        lastTime:0,      //上次改变时间
        inputTimes:0,//累计连续输入次数
        searchTimeOut:0,
        thatIsTM:false,       //是否为条形码

        $waiting:false,
        $waitTime:2,
        jump2Goods: function () {

        },
        mustOut: function () {

        }



    });

});
